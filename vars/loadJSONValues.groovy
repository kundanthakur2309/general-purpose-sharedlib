#!/usr/bin/env groovy

def call(Object pipeVars)
{
    configFileProvider([configFile(fileId: '881c31bb-c2d4-46be-b5b1-086667d06c07', variable: 'pipeline_config')]) {
        script{
            pipeVars = readJSON file: pipeline_config
            echo "===> got deployment data: ${pipeVars}"
            if ( pipeVars.applicationSettings.appName == null || pipeVars.applicationSettings.appName == ''){
                error('Stopped because appName is not in pipeline.json')
            }
            if (pipeVars.generalSettings.mode != 'buildOnly' && pipeVars.generalSettings.mode != 'build2Nexus' && pipeVars.generalSettings.mode != 'CICD' && pipeVars.generalSettings.mode != 'smokeTestOnly' && pipeVars.generalSettings.mode != 'deployOnly'){
                error('Stopped because Invalid mode from  pipeline.json. Valid values are buildOnly (meaning to compile and pacakge), build2Nexus (meaning buildOnly + load to Nexus snapshot repo), CICD (vuild2Nexus + deploy + smoektest), smokeTestOnly (meaning run smokeTest only), and deployOnly (meaning only deploy Module) ')
            }
            if( pipeVars.generalSettings.model == null || pipeVars.generalSettings.model == ''){
                error('Stopped because model value is null or empty')
            }
            if(pipeVars.generalSettings.model.toLowerCase() != "cloudops" && pipeVars.generalSettings.model.toLowerCase() != "weblogic" && pipeVars.generalSettings.model.toLowerCase() != "pcf"){
                error("Stopped because of an invalid model value: ${pipeVars.generalSettings.model}. Value must be either CloudOps, WebLogic or PCF.")
            }
        }
    }
    return pipeVars
}
